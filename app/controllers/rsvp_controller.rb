class RsvpController < ApplicationController
  def new
    @rsvp = Rsvp.new
  end
  
  def show
    @rsvp = Rsvp.find(params[:id])
  end
  
  def create
    @rsvp = Rsvp.new(rsvp_params)
    if @rsvp.save
      flash[:success] = "Thanks for RSVPing, it's been saved!"
      redirect_to 
    else 
      render 'new'
    end
  end
  
  def created
    
  end
  
  private
    def rsvp_params
      params.require(:rsvp).permit(:name,:email,:attending,:FoodPreference,:additionalInfo)
    end
  
  
end
