class StaticPagesController < ApplicationController
  def home
    @pageClass = 'homepage'
  end
  
  def details
    @pageClass = 'details'
    @partialViewClass = 'container greyBackground'
  end
  
  def contact
  end
  
  def register
  end
  
  def accomodation
  end
  
  def photos
  end
  
  def rsvp
  end
  
end
