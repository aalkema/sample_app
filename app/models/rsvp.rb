class Rsvp < ActiveRecord::Base
  @FOODS = ['prime rib', 'chicken', 'vegetarian']
  validates :name, presence: true
  validates :FoodPreference, presence: true,
                            :inclusion => @FOODS
end