Rails.application.routes.draw do
  get 'rsvp/new'

  root             'static_pages#home'
  get 'details'   => 'static_pages#details'
  get 'contact' => 'static_pages#contact'
  get 'register'=> 'static_pages#register'
  get 'accomodation' => 'static_pages#accomodation'
  get 'photos' => 'static_pages#photos'
  get 'rsvps' => 'rsvp#new'
  post 'rsvps' => 'rsvp#create'
  get 'created' => 'rsvp#created'
  resources :rsvp
end
