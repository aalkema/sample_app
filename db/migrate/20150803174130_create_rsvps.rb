class CreateRsvps < ActiveRecord::Migration
  def change
    create_table :rsvps do |t|
      t.string :name
      t.string :email
      t.boolean :attending
      t.string :additionalInfo

      t.timestamps null: false
    end
  end
end
